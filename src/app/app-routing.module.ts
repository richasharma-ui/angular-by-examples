import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TopicsComponent } from './topics/topics.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

const routes: Routes = [
  { path: '',   redirectTo: '/topics', pathMatch: 'full' },
  { path: 'topics', component: TopicsComponent },
  { path: 'topics/login-register', loadChildren: () =>
   import('./topics/login-register/login-register.module').then(m => m.LoginRegisterModule) },
  { path: 'topics/routing-navigation', loadChildren: () =>
   import('./topics/routing-navigation/routing-navigation.module').then(m => m.RoutingNavigationModule) },
  { path: 'topics/life-cycle-hooks', loadChildren: () =>
   import('./topics/life-cycle-hooks/life-cycle-hooks.module').then(m => m.LifeCycleHooksModule) },
  { path: 'topics/directives', loadChildren: () =>
   import('./topics/directives/directives.module').then(m => m.DirectivesModule) },
  { path: 'topics/pipes', loadChildren: () =>
   import('./topics/pipes/pipes.module').then(m => m.PipesModule) },
  { path: 'topics/component-communication', loadChildren: () =>
   import('./topics/component-communication/component-communication.module').then(m => m.ComponentCommunicationModule) },
  { path: 'topics/services', loadChildren: () =>
   import('./topics/services/services.module').then(m => m.ServicesModule) },
  { path: 'topics/http', loadChildren: () => import('./topics/http/http.module').then(m => m.HttpModule) },
  { path: 'topics/observable-rxjs', loadChildren: () =>
   import('./topics/observable-rxjs/observable-rxjs.module').then(m => m.ObservableRxjsModule) },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
