import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ObservableRxjsComponent } from './observable-rxjs.component';

const routes: Routes = [{ path: '', component: ObservableRxjsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ObservableRxjsRoutingModule { }
