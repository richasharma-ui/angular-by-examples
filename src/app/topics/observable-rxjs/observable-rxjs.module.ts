import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ObservableRxjsRoutingModule } from './observable-rxjs-routing.module';
import { ObservableRxjsComponent } from './observable-rxjs.component';


@NgModule({
  declarations: [ObservableRxjsComponent],
  imports: [
    CommonModule,
    ObservableRxjsRoutingModule
  ]
})
export class ObservableRxjsModule { }
