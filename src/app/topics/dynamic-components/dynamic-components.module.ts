import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DynamicComponentsRoutingModule } from './dynamic-components-routing.module';
import { DynamicComponentsComponent } from './dynamic-components.component';


@NgModule({
  declarations: [DynamicComponentsComponent],
  imports: [
    CommonModule,
    DynamicComponentsRoutingModule
  ]
})
export class DynamicComponentsModule { }
