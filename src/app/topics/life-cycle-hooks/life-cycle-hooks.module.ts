import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LifeCycleHooksRoutingModule } from './life-cycle-hooks-routing.module';
import { LifeCycleHooksComponent } from './life-cycle-hooks.component';


@NgModule({
  declarations: [LifeCycleHooksComponent],
  imports: [
    CommonModule,
    LifeCycleHooksRoutingModule
  ]
})
export class LifeCycleHooksModule { }
