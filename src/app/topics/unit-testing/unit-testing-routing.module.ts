import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UnitTestingComponent } from './unit-testing.component';

const routes: Routes = [{ path: '', component: UnitTestingComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnitTestingRoutingModule { }
