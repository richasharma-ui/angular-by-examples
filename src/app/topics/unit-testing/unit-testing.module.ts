import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UnitTestingRoutingModule } from './unit-testing-routing.module';
import { UnitTestingComponent } from './unit-testing.component';


@NgModule({
  declarations: [UnitTestingComponent],
  imports: [
    CommonModule,
    UnitTestingRoutingModule
  ]
})
export class UnitTestingModule { }
